import React from 'react'

const ListElement = (props) => {
  let {user, opened} = props
  let rotation = opened ? 90 : 0
  let arrowStyle = {transform: "rotate(" +  rotation + "deg)"}

  return (
    <div className="user" onClick={props.onClick}>
      <div className="user-block">
        <div>
          <div className="name">{user.name}</div>
          <div className="email">{user.email}</div>
        </div>
        <div style={arrowStyle}>▶</div>
      </div>
      {opened && <div className="address" >{formatAddress(user.address)}</div>}
    </div>
  )

}

export default ListElement

function formatAddress(address) {
  return (<div>
    <text style={{ fontWeight: 'bold' }}>{'Adress: '}</text>
    <text>
      {address.street +
        ', ' +
        address.suite +
        ', ' +
        address.city +
        ', ' +
        address.zipcode}
    </text>
  </div>)
}
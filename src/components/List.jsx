import React, {useState} from 'react'
import ListElement from './ListElement'

const List = props => {
  let { data } = props;
  let [myuser, setMyuser] = useState(null);

  let elems = data.map((user, index) => {
    return (
      <ListElement
        key={user.id}
        user={user}
        opened={user.id === myuser ? true : false}
        onClick={() => {setMyuser(user.id)}}
      />
    );
  });

  return <div>{elems}</div>;
};
export default List;

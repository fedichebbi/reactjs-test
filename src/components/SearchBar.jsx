import React from 'react'

const SearchBar = (props) => {
  return (
    <div className="searchbar">
      <input type="text" placeholder="Find by name" onInput={(e) => {props.onInput(e.target.value)}}/>
      <div className="count">
      {props.data.length} People
      </div>
    </div>
  )
}

export default SearchBar
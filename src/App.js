import React, { useState, useEffect } from 'react'
import './App.css';

import Header from './components/Header'
import SearchBar from './components/SearchBar'
import List from './components/List'

const App = () => {

  let [data, setData] = useState([])
  let [initialData, setinitialData] = useState([])

  useEffect(() => {
    const url = 'http://jsonplaceholder.typicode.com/users';
    fetch(url)
      .then(response => response.json())
      .then(res => {
        setData(res)
        setinitialData(res)
      });
  }, [])

  function filterData(str = "") {
    // ICI faire la fonction de filtre
    let filteredData = initialData.filter(person =>
      person.name.toLowerCase().includes(str.toLowerCase()),
    );
    setData(data => filteredData);
  }

  return (
    <div className="App">
      <Header />
      <SearchBar data={data} onInput={filterData} />
      <List data={data} />
    </div>)

}

export default App

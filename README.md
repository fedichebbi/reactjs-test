TEST REACTJS
============

MISE EN PLACE
-------------
Il faut avoir nodejs et npm installé

* se rendre dans le dossier du test, ouvrir dans le terminal
* `npm install`
* `npm start` permet de lancer un serveur de dev


CONSIGNES
---------

![Screenshot](screenshot.png)

Il s'agit d'une page présentant une liste de personnes, sous forme de blocs. Un champ de texte permet de filtrer les personnes au cours de la frappe. Chaque bloc utilisateur peut être ouvert/déplié, ce qui affiche l'adresse de l'utilisateur (cf screenshot, 2eme personne)

1. Récupérer la liste dynamiquement depuis cette url : http://jsonplaceholder.typicode.com/users
2. Afficher le compte du nombre de gens affichés (remplacer [COUNT] dans SerachBar.jsx)
3. Faire la fonction de filtre des utilisateurs (enlever les utilisateurs dont le nom ne contient pas la chaine tapée, ne pas tenir compte des majuscules/minuscules)
4. Gérer la fonction de dépliage des blocs. Attention, on ne doit pouvoir déplier qu'un seul bloc à la fois (le bloc précédemment ouvert se replie).
5. Il faut formatter l'adresse de l'utilisateur afin qu'elle soit identique au screenshot ci-dessous (cf. méthode `formatAddress` dans `ListElement.jsx`).